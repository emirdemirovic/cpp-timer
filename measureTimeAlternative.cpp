#include <iostream>
#include "timerAlternative.hpp"

unsigned long long fib_memo(unsigned n)
{
    Timer timer;
	int i = 2;
	unsigned long long prev = 0, tmp = 1, next = 0;
	while ( i <= n )
	{
	    next = prev + tmp;
		prev = tmp;
		tmp = next;
		++i;
	}

	return tmp;
}

unsigned long long fibonacci(unsigned n)
{
    if (n < 2) 
        return n;
    return fibonacci(n-1) + fibonacci(n-2);
}

int main()
{
    Timer memoFibonacciTimer;
    memoFibonacciTimer.Start();
    auto result1 = fibonacci(40);
    memoFibonacciTimer.Stop();
    std::cout << "Fibonacci using memoization (w/o array):\n";
    memoFibonacciTimer.PrintDuration();
    
    Timer recursiveFibonacciTimer;
    recursiveFibonacciTimer.Start();
    auto result2 = fib_memo(40);
    recursiveFibonacciTimer.Stop();
    std::cout << "Recursive Fibonacci: \n";
    recursiveFibonacciTimer.PrintDuration();

    return 0;
}
# CPP execution timer (timer.hpp)

**How to use:**

1.    `#include "timer.hpp" `
1.   `Timer timer;` Default initialize an object and from the moment it initializes to the moment it dies from a destructor it will count the duration. 
    
1. OPTIONAL: Calling the Start(), Stop() and GetDuration() methods. Start(), as the name implies, will start the countdown, while Stop() will stop it. If you want, you can get the duration by calling the GetDuration() method. **You don't need to call the GetDuration() method for it to show the duration at the end of the execution because of the destructor of the created object.**

See the [example.cpp](https://gitlab.com/emirdemirovic/cpp-timer/blob/master/example.cpp)

# timerAlternative.hpp

`#include "timerAlternative.hpp"`

You **NEED** to call the Start() and Stop() methods to calculate the duration.

Printing the duration is done by calling the PrintDuration() method.

See the [measureTimeAlternative.cpp](https://gitlab.com/emirdemirovic/cpp-timer/blob/master/measureTimeAlternative.cpp)

# NOTE

**THIS IS NOT A BENCHMARKING TOOL, IT JUST GIVES A ROUGH ESTIMATE OF AN EXECUTION TIME!**

#ifndef _TIMER
#define _TIMER
#include <iostream>
#include <chrono>
#include <thread>

struct Timer
{
    std::chrono::high_resolution_clock::time_point start, end;
    std::chrono::duration<float> duration;
    float ms = 0;
    Timer()
    {
        start = std::chrono::high_resolution_clock::now();
    }
    ~Timer()
    {
        end = std::chrono::high_resolution_clock::now();
        duration = end - start;
        
        ms = duration.count() * 1000.0f;
        std::cout << "Duration: " << ms << "ms\n";
    }
    void Start();
    void Stop();
    float GetDuration();
};

void Timer::Start()
{
    start = std::chrono::high_resolution_clock::now();

}
void Timer::Stop()
{
    end = std::chrono::high_resolution_clock::now();

}
float Timer::GetDuration()
{
    duration = end - start;
    ms = duration.count() * 1000.0f;

    return ms;
}
#endif

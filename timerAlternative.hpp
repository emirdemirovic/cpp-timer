#ifndef _TIMER_ALTERNATIVE_
#define _TIMER_ALTERNATIVE_
#include <iostream>
#include <chrono>
#include <thread>

struct Timer
{
    std::chrono::high_resolution_clock::time_point start, end;
    std::chrono::duration<float> duration;
    float ms = 0;
    void Start();
    void Stop();
    float GetDuration();
    void PrintDuration();
};

void Timer::Start()
{
    start = std::chrono::high_resolution_clock::now();

}
void Timer::Stop()
{
    end = std::chrono::high_resolution_clock::now();

}
float Timer::GetDuration()
{
    duration = end - start;
    ms = duration.count() * 1000.0f;
    return ms;
}
void Timer::PrintDuration()
{   
    duration = end - start;
    ms = duration.count() * 1000.0f;
    std::cout << "Duration: " << ms << "ms\n";
}
#endif

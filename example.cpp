#include <iostream>
#include "timer.hpp"

unsigned long long fib_memo(unsigned n)
{
    Timer memoFibonacciTimer;
	int i = 2;
	unsigned long long prev = 0, tmp = 1, next = 0;
	while ( i <= n )
	{
	    next = prev + tmp;
		prev = tmp;
		tmp = next;
		++i;
	}

	return tmp;
}

unsigned long long fibonacci(unsigned n)
{
    if (n < 2) 
        return n;
    return fibonacci(n-1) + fibonacci(n-2);
}

int main()
{
    Timer recursiveFibonacciTimer;
    auto result1 = fibonacci(40);
    std::cout << "Fibonacci using memoization (w/o array):\n";
    auto result2 = fib_memo(40);
    std::cout << "Recursive Fibonacci: \n";

    return 0;
}